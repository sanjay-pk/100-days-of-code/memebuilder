const imageMeme = document.getElementById("memeImg")
const nameMeme = document.getElementById("memeName")
const btn = document.getElementById("buttonMeme")
const cardDiv = document.getElementById('cardDiv')
const nextBtn = document.getElementById('nextBtn')
const backBtn = document.getElementById('backBtn')
let currentMemeIndex = 0;
// takes memes data and append to this let. we can use this outside of the function .
let memes = [];

btn.addEventListener('click', fetchData)

async function fetchData() {
    try {
    // calling the api and get the data
        const response = await fetch('https://api.imgflip.com/get_memes');
        const memeData = await response.json();
        memes = memeData.data.memes;
        
        if (currentMemeIndex >= memes.length) {
            currentMemeIndex = 0;
        }
    // changing display property 
        btn.style.display = 'none'
        cardDiv.style.display = 'block'   
        nextBtn.style.display = 'block'
        backBtn.style.display = 'block'
    // variable to save memes. currentindex
        const currentMeme = memes[currentMemeIndex];
    // append hte image and title as per index
        imageMeme.src = currentMeme.url;
        nameMeme.textContent = currentMeme.name;
    } catch (error) {
        console.error('Error fetching data:', error);
    }
}

// Next button click event listener
nextBtn.addEventListener('click', () => {
    currentMemeIndex = (currentMemeIndex + 1) % memes.length;
    fetchData();
});

// Previous button click event listener
backBtn.addEventListener('click', () => {
    currentMemeIndex = (currentMemeIndex - 1 + memes.length) % memes.length;
    fetchData();
});
